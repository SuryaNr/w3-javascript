new gridjs.Grid({
  columns: ["Name", "Email"],
  pagination: {
  limit: 2,
},
  data: [
    ["Eduwork", "eduwork@example.com"],
    ["Surya", "sur@gmail.com"],
    ["ivy", "ivy@gmail.com"],
    ["Khai", "khaii@gmail.com"],
    ["Weyn", "weyn@mail.com"],
  ],
  sort: true,
  fixedHeader: true,
  height: '200px',

}).render(document.getElementById("wrapper"));